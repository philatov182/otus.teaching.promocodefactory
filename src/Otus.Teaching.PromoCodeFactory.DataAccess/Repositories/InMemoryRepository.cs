﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
	        if (id == Guid.Empty)
	        {
		        throw new ArgumentNullException($"{nameof(id)}");
	        }

	        return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> CreateAsync(T entity)
        {
	        if (entity == null)
	        {
		        throw new ArgumentNullException($"{nameof(entity)}");
	        }

	        return Task.Run(() =>
	        {
		        var dataList = Data.ToList();

		        if (dataList.Any(x => x.Id.Equals(entity.Id)))
		        {
			        throw new ArgumentException($"{nameof(entity.Id)} is already exists in the {nameof(Data)}");
		        }

		        dataList.Add(entity);
		        Data = dataList.ToArray();
				return true;
			});
        }

		public Task<bool> DeleteByIdAsync(Guid id)
		{
			if (id == Guid.Empty)
			{
				throw new ArgumentNullException($"{nameof(id)}");
			}

			return Task.Run(() =>
			{
				var dataList = Data.ToList();

				var element = dataList.FirstOrDefault(x => x.Id.Equals(id));
				if (element == null)
				{
					throw new ArgumentOutOfRangeException($"{nameof(id)} is not in the {nameof(Data)}");
				}

				dataList.Remove(element);
				Data = dataList.ToArray();
				return true;
			});
		}

        public Task<bool> UpdateAsync(T entity) 
        {
	        if (entity == null)
	        {
		        throw new ArgumentNullException($"{nameof(entity)}");
	        }

	        return Task.Run(() =>
	        {
		        var dataList = Data.ToList();

		        var index = dataList.FindIndex(x => x.Id.Equals(entity.Id));
		        if (index == -1)
		        {
			        throw new ArgumentOutOfRangeException($"{nameof(entity.Id)} is not in the {nameof(Data)}");
		        }

		        dataList[index] = entity;
		        Data = dataList.ToArray();
		        return true;
	        });
        }
    }
}